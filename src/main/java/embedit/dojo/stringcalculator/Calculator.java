package embedit.dojo.stringcalculator;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;

public class Calculator {

    public String add(String input) throws IOException {
        return new InputParser(input).parseInput(new StringReader(input))
                                     .reduce(BigDecimal.ZERO, BigDecimal::add)
                                     .toString();
    }

}
