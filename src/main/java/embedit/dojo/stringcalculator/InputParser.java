package embedit.dojo.stringcalculator;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.lang.Character.isDigit;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Spliterators.spliteratorUnknownSize;

class InputParser {
    private static final ParsingState PARSING_CONFIG = new ParsingConfig();
    private static final ParsingState PARSING_NUMBER = new ParsingNumber();

    //      //sep\n1.1sep1.2sep1.3
    //      ^
    // Start      -> ConfigDef('/') | Number('[0-9]') | EndEmpty (<EOL>) | Error(*)
    // ConfigDef  -> ConfigDef('//') | Error(*)
    // Number     -> Number([0-9.]) | Separator(<isPossibleSeparatorStart>) | EndSum(EOL) | Error(*)
    // Separator  -> Separator | Number | Error

    private final List<String> separators = new LinkedList<>(Arrays.asList(",", "\n"));

    private final StringReader input;
    private       ParsingState currentState = new Start();

    InputParser(StringReader input) {
        this.input = input;
    }

    void setState(ParsingState state) {
        this.currentState = state;
    }

    StringReader getInput() {
        return input;
    }

    Stream<BigDecimal> parseInput() throws IOException, ParsingException {
        final Supplier<Optional<BigDecimal>> numberSupplier = () -> {
            try {
                return this.currentState.parse(this); //returns: number, (currentState, input)
            } catch (IOException e) {
                throw new ParsingException("IO error", e);
            }
        };
        return StreamSupport.stream(spliteratorUnknownSize(new ParsingNumberIterator(numberSupplier), 0), false);
    }


    static class ParsingNumberIterator implements Iterator<BigDecimal> {

        private final Supplier<Optional<BigDecimal>> numberSupplier;
        private       BigDecimal                     nextNumber;

        ParsingNumberIterator(Supplier<Optional<BigDecimal>> numberSupplier) {
            this.numberSupplier = numberSupplier;
            this.nextNumber = numberSupplier.get().orElse(null);
        }

        @Override
        public boolean hasNext() {
            return nonNull(nextNumber);
        }

        @Override
        public BigDecimal next() {
            if (isNull(nextNumber)) {
                throw new NoSuchElementException();
            }
            try {
                return nextNumber;
            } finally {
                nextNumber = numberSupplier.get().orElse(null);
            }
        }
    }


    interface ParsingState {
        Optional<BigDecimal> parse(InputParser parserContext) throws IOException;

        boolean isEnd();
    }

    private class Start implements ParsingState {

        @Override
        public Optional<BigDecimal> parse(InputParser parserContext) throws IOException {
            final int aByte = input.read();
            final char ch = (char) aByte;
            if (ch == '/') {
                return parserContext.parseUsingState(PARSING_CONFIG);
//                return parserContext.currentState.parse(parserContext);
            } else if (isDigit(ch) || ch == '.') {
                parserContext.setState(PARSING_NUMBER);
                return parserContext.currentState.parse(parserContext);
            } else if (aByte == -1) {
                return Optional.empty();
            } else {
                throw new IllegalArgumentException();
            }
        }

        @Override
        public boolean isEnd() {
            return false;
        }
    }

    private static class ParsingConfig implements ParsingState {
        @Override
        public Optional<BigDecimal> parse(InputParser parserContext) throws IOException {
            return null;
        }

        @Override
        public boolean isEnd() {
            return false;
        }


    }

    private static class ParsingNumber implements ParsingState {
        @Override
        public Optional<BigDecimal> parse(InputParser parserContext) throws IOException {
            return null;
        }

        @Override
        public boolean isEnd() {
            return false;
        }
    }

    private class ParsingException extends RuntimeException {

        ParsingException(String message, IOException e) {
            super(message, e);
        }
    }

}



