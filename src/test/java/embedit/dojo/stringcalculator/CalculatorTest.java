package embedit.dojo.stringcalculator;

import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class CalculatorTest {

    private Calculator calculator = new Calculator();

    @Test
    public void should_return_zero_when_empty_string() throws Exception{
        assertThat(new Calculator().add(""), is("0"));
    }

    @Test
    public void should_return_the_same_number_if_input_is_one_number() throws IOException {
        assertThat(new Calculator().add("1"), is("1.0"));
    }

    @Test
    public void should_return_the_same_number_if_input_is_one_decimal_number() throws IOException {
        assertThat(new Calculator().add("1.1"), is("1.1"));
    }

    @Test
    public void should_return_sum_of_2_decimals() throws IOException {
        assertThat(calculator.add("1.1,2.1"), is("3.2"));
    }

    @Test
    public void should_return_sum_of_decimal_and_int() throws IOException {
        assertThat(calculator.add("1.1,2"), is("3.1"));
    }

    @Test
    public void should_return_sum_of_5_decimals() throws IOException {
        assertThat(calculator.add("1.1,2.1,3.1,4.1,5.25"), is("15.65"));
    }

    @Test
    public void should_return_sum_of_3_ints_with_newline_as_separator() throws IOException {
        assertThat(calculator.add("1\n2,3"), is("6.0"));
    }

    @Test
    public void should_return_error_message_when_no_number_between_separators_comma_first() throws IOException {
        assertThat(calculator.add("175.2,\n35"), is("Number expected but '\n' found at position 6."));
    }

    @Test
    public void should_return_error_message_when_no_number_between_separators_newline_first() throws IOException {
        assertThat(calculator.add("175.2\n,35"), is("Number expected but ',' found at position 6."));
    }

    @Test
    public void should_return_error_message_when_separator_at_the_end() throws IOException {
        assertThat(calculator.add("175.2,35,"), is("Number expected but EOF found"));
    }

    @Test
    public void should_parse_using_custom_delimiter() throws IOException {
        assertThat(calculator.add("//;\n1;2"), is("3.0"));
    }

    @Test
    public void should_parse_using_2_multi_character_delimiters() throws IOException {
        assertThat(calculator.add("//sep\n2sep3"), is("5.0"));

        //      //sep\n1.1sep1.2sep1.3
        //      ^
        // Start      -> ConfigDef('/') | Number('[0-9]') | EndEmpty (<EOL>) | Error(*)
        // ConfigDef  -> ConfigDef('//') | Error(*)
        // Number     -> Number([0-9.]) | Separator(<isPossibleSeparatorStart>) | EndSum(EOL) | Error(*)
        // Separator  -> Separator | Number | Error
    }
}
